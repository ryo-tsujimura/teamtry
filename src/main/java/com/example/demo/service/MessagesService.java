package com.example.demo.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Message;
import com.example.demo.repository.MessageRepository;

@Service
public class MessagesService {
	@Autowired
	MessageRepository messageRepository;

	//メッセージ全件取得
	public List<Message> findAllMessage() {
		return messageRepository.findAllByOrderByCreatedDateDesc();
	}
	public void saveMessage(Message message) {
		messageRepository.saveAndFlush(message);
	}
	public void deleteMessage(Integer id) {
		Message message = messageRepository.findById(id).get();
		messageRepository.delete(message);
	}
	public List<Message> findByCreatedDateAndCategory(String startDate, String endDate, String searchCategory) {
		Timestamp start = changeStartDate(startDate);
		Timestamp end = changeEndDate(endDate);

		return messageRepository.findByCreatedDateBetweenAndCategoryContainingOrderByCreatedDateDesc(start, end,
				searchCategory);
	}

	public Timestamp changeStartDate(String startDate) {
		Timestamp start;
		if (!startDate.isEmpty()) {
			return start = Timestamp.valueOf(startDate + " 00:00:00");
		} else {
			return start = Timestamp.valueOf("2020-07-01 00:00:00");
		}
	}

	public Timestamp changeEndDate(String endDate) {
		Timestamp end;
		if (!endDate.isEmpty()) {
			return end = Timestamp.valueOf(endDate + " 23:59:59");
		} else {
			return end = new Timestamp(System.currentTimeMillis());
		}
	}
}
