package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;


@Service
public class CommentsService {
	@Autowired
	CommentRepository commentRepository;

	//コメント全件取得
	public List<Comment> findAllComment(){
		return commentRepository.findAll();
	}

	//コメント登録
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	//コメント削除
	public void deleteComment(Integer id) {
		Comment comment = commentRepository.findById(id).get();
		commentRepository.delete(comment);
	}
}
