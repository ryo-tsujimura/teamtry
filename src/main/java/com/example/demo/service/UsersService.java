package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;

@Service
public class UsersService {
	@Autowired
	UserRepository userRepository;
	public void saveUser(User user) {
		userRepository.saveAndFlush(user);
	}

	public User loginUser(String account, String password) {
		return userRepository.findByAccountAndPassword(account, password);
	}

	public List<User> findAllUser(){
		return userRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
	}

	public User editUser(Integer id) {
		User user = (User)userRepository.findById(id).orElse(null);
		return user;
	}

	public User findOne(Integer id) {
		return userRepository.getOne(id);
	}

	public Optional<User> findById(int id) {
		return userRepository.findById(id);
	}

}
