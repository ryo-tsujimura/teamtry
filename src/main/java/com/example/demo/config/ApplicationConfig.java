package com.example.demo.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.filter.AdministarFilter;
import com.example.demo.filter.LoginFilter;

@Configuration
public class ApplicationConfig {
    @Bean
    public FilterRegistrationBean<LoginFilter> longinFilter1(){

        FilterRegistrationBean<LoginFilter> bean = new FilterRegistrationBean<LoginFilter>(new LoginFilter());
        //フィルターをかけるURLを指定
        bean.addUrlPatterns("/", "/try", "/message", "/user", "/edit/*", "/signup");
        //フィルターの実行順序を指定
        bean.setOrder(1);
        return bean;
    }

    @Bean
    public FilterRegistrationBean<AdministarFilter> administarFilter2(){
    	FilterRegistrationBean<AdministarFilter> bean = new FilterRegistrationBean<AdministarFilter>(new AdministarFilter());
    	bean.addUrlPatterns("/user", "/edit/*", "/signup");
        bean.setOrder(2);
        return bean;
    }
}
