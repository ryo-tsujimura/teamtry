package com.example.demo.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {
	public List<Message> findAllByOrderByCreatedDateDesc();

	public List<Message> findByCreatedDateBetweenAndCategoryContainingOrderByCreatedDateDesc(Timestamp startDate,
			Timestamp endDate, String searchCategory);
}
